package de.evilshark.sharkessentials.commands;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import de.evilshark.sharkessentials.SharkEssentials;

public class BackPackCommand implements CommandExecutor {

	private HashMap<UUID, Inventory> inventorys = new HashMap<UUID, Inventory>();
	
	String name = "�5BackPack";
	
	
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				
				if(inventorys.get(p.getUniqueId()) == null) {
					
					p.sendMessage(SharkEssentials.se+"Dein virtuelles Inventar wird erstellt!");
					Inventory VInv = Bukkit.createInventory(p, 18, name);
					p.sendMessage(SharkEssentials.se+"Um es zu �ffnen gebe /bp ein!");
					inventorys.put(p.getUniqueId(), VInv);
				} else {
					p.sendMessage(SharkEssentials.se+"Dein BackPack hat sich ge�ffnet!");
					p.openInventory(inventorys.get(p.getUniqueId()));
					
				}
				
			} else if(args.length == 1) {
				
				Player p2 = Bukkit.getPlayer(args[0]);
				
				if(p.hasPermission("sharkessentials.commands.backpack.other")) {
				
				if(p2 != null && p2.isOnline()) {
					
					if(inventorys.get(p2.getUniqueId()) == null) {
						p.sendMessage(SharkEssentials.se+"�4FEHLER�8: �cDieser Spieler hat noch kein Backpack erstellt!");
					} else {
						p.sendMessage(SharkEssentials.se+"Du siehst das BackPack von �5"+p2.getName());
						p.openInventory(inventorys.get(p2.getUniqueId()));
					}
				} else {
					p.sendMessage(SharkEssentials.np);
				}
					
				} else {
					p.sendMessage(SharkEssentials.op);
				}
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}

}
