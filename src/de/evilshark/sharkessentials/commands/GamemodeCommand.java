package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class GamemodeCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 1) {
					if(args[0].equalsIgnoreCase("0")) {						
						p.sendMessage(SharkEssentials.se+"Du bist nun im �5Survival �8Modus!");
						p.setGameMode(GameMode.SURVIVAL);				
					} else if(args[0].equalsIgnoreCase("1")) {
						p.sendMessage(SharkEssentials.se+"Du bist nun im �5Creative �8Modus!");
						p.setGameMode(GameMode.CREATIVE);
					} else if(args[0].equalsIgnoreCase("2")) {
						p.sendMessage(SharkEssentials.se+"Du bist nun im �5Adventure �8Modus!");
						p.setGameMode(GameMode.ADVENTURE);
					} else if(args[0].equalsIgnoreCase("3")) {
						p.sendMessage(SharkEssentials.se+"Du bist nun im �5Spectator �8Modus!");
						p.setGameMode(GameMode.SPECTATOR);
					} else {
						p.sendMessage(SharkEssentials.af);
						
					}
					
					
				} else if(args.length == 2) {
					Player p2 = Bukkit.getPlayer(args[1]);
					if(p2 != null && p2.isOnline()) {
						if(args[0].equalsIgnoreCase("0")) {
							p.sendMessage(SharkEssentials.se+"Der Spieler �6"+p2.getName()+" �8ist nun im �5Survival �8Modus!");
							p2.setGameMode(GameMode.SURVIVAL);
							p2.sendMessage(SharkEssentials.se+"Du bist nun im �5Survival �8Modus!");
						} else if(args[0].equalsIgnoreCase("1")) {
							p.sendMessage(SharkEssentials.se+"Der Spieler �6"+p2.getName()+" �8ist nun im �5Creative �8Modus!");
							p2.setGameMode(GameMode.CREATIVE);
							p2.sendMessage(SharkEssentials.se+"Du bist nun im �5Creative �8Modus!");							
						} else if(args[0].equalsIgnoreCase("2")) {
							p.sendMessage(SharkEssentials.se+"Der Spieler �6"+p2.getName()+" �8ist nun im �5Adventure �8Modus!");
							p2.setGameMode(GameMode.ADVENTURE);
							p2.sendMessage(SharkEssentials.se+"Du bist nun im �5Adventure �8Modus!");							
						} else if(args[0].equalsIgnoreCase("3")) {
							p.sendMessage(SharkEssentials.se+"Der Spieler �6"+p2.getName()+" �8ist nun im �5Spectator �8Modus!");
							p2.setGameMode(GameMode.SPECTATOR);
							p2.sendMessage(SharkEssentials.se+"Du bist nun im �5Spectator �8Modus!");							
						} else {
							p.sendMessage(SharkEssentials.af);
						}	
					} else {
						p.sendMessage(SharkEssentials.se+"�4FEHLER�8: Der Spieler ist nicht online!");
						return true;
					}
					
				} else {
					p.sendMessage(SharkEssentials.af);
				}
			} else {
				cs.sendMessage(SharkEssentials.cc);
		}
			
		
		
		
		
		
		return true;
	}

}
