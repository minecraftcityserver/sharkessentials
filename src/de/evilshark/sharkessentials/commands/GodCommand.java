package de.evilshark.sharkessentials.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import de.evilshark.sharkessentials.SharkEssentials;

public class GodCommand implements CommandExecutor, Listener {

	public static ArrayList<String> god = new ArrayList<String>();
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				if(god.contains(p.getName())) {
					p.sendMessage(SharkEssentials.se+"Du bist nun nicht mehr im God Mode!");
					god.remove(p.getName());
				} else {
					p.sendMessage(SharkEssentials.se+"Du bist nun im God Mode!");
					god.add(p.getName());
			
				}
			} else if(args.length == 1) {
				Player p2 = Bukkit.getPlayer(args[0]);
				if(p2 != null && p2.isOnline()) {
					if(god.contains(p2.getName())) {
						p.sendMessage(SharkEssentials.se+"Der Spieler �6"+p2.getName()+" �8ist nun nicht mehr im God Mode!");
						god.remove(p2.getName());
					} else {
						p.sendMessage(SharkEssentials.se+"Der Spieler �6"+p2.getName()+" �8ist im God Mode!");
						god.add(p2.getName());
					}
					
				} else {
					p.sendMessage(SharkEssentials.se+"�4FEHLER�8: Der Spieler wurde nicht gefunden!");
				}
				
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		
		return true;
	}
	
	

}
