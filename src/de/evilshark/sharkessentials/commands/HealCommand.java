package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class HealCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				p.sendMessage(SharkEssentials.se+"Du wurdest geheilt!");
				p.setHealth(20);
				p.setFoodLevel(20);
			} else if(args.length == 1) {
				Player p2 = Bukkit.getPlayer(args[0]);
				if(p2 != null && p2.isOnline()) {
					p2.sendMessage(SharkEssentials.se+"Du wurdest geheilt!");
					p.sendMessage(SharkEssentials.se+"Du hast den Spieler �6"+p2.getName()+" �8wurde geheilt!");
					p.setHealth(20);
					p.setFoodLevel(20);
				} else {
					p.sendMessage(SharkEssentials.se+"�4FEHLER�8: Der Spieler wurde nicht gefunden!");
				}
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}

}
