package de.evilshark.sharkessentials.commands;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class MSGCommand implements CommandExecutor {
	public static HashMap<UUID, UUID> lastSentMessages = new HashMap<UUID, UUID>();
	String msg = "";
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length >= 2) {
				Player p2 = Bukkit.getPlayer(args[0]);
				
				if(p2 != null && p2.isOnline() ) {
					
					for(int i = 1; i < args.length; i++) {
						msg = msg + args[i] + " ";
					}
					p.sendMessage("�6MSG �8>> �5Du �8--> �5"+p2.getName()+" �8>> "+msg);
					p2.sendMessage("�6MSG �8>> �5"+p.getName()+" �8--> �5Du �8>> "+msg);
					
					msg = "";
					
				} else {
					p.sendMessage(SharkEssentials.op);
				}
				
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}

}
