package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class KillCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length <2) {
				if(args.length == 0) {
					p.sendMessage(SharkEssentials.se+"Du hast dich selbst get�tet!");
					p.setHealth(0.0);
					p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 1, 1);
				} else if(args.length == 1) {
					Player p2 = Bukkit.getPlayer(args[0]);
					if(p2 != null && p2.isOnline()) {
						p2.setHealth(0.0);
						p2.sendMessage(SharkEssentials.se+"Der Spieler �6"+p.getName()+" �8hat dich get�tet!");
						p.sendMessage(SharkEssentials.se+"Du hast den Spieler �6"+p2.getName()+" �8get�tet!");
						p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 1, 1);
						p.playSound(p2.getLocation(), Sound.BLOCK_ANVIL_USE, 1, 1);
					} else {
						
						p.sendMessage(SharkEssentials.se+"�4FEHLER: �8Der Spieler wurde nicht gefunden!");
						
						return true;
					}
				}
				
				
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
		}
	
	}
