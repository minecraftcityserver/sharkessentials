package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class FeedCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				p.sendMessage(SharkEssentials.se+"Dein Hunger wurde aufgefüllt!");
				p.setFoodLevel(26);
			} else if(args.length == 1) {
				Player p2 = Bukkit.getPlayer(args[0]);
				
				if(p2 !=null && p2.isOnline()) {
					p2.sendMessage(SharkEssentials.se+"Dein Hunger wurde aufgefüllt!");
					p.sendMessage(SharkEssentials.se+"Der Hunger von §5"+p2.getName()+" §8wurde aufgefüllt!");
					p2.setFoodLevel(26);
;				} else {
					p.sendMessage(SharkEssentials.op);
				}
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}

		
		
		return true;
	}

}
