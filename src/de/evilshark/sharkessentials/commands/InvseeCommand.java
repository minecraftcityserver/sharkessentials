package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import de.evilshark.sharkessentials.SharkEssentials;

public class InvseeCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 1) {
				
				Player p2 = Bukkit.getPlayer(args[0]);
				
				if(p2 != null && p2.isOnline()) {
					
					Inventory TargetInv = p2.getInventory();
					
					p.openInventory(TargetInv);
					p.sendMessage(SharkEssentials.se+"Du hast das Inventar von �5"+p2.getName()+" �8ge�ffnet!");
					
				} else {
					p.sendMessage(SharkEssentials.op);
				}
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}

}
