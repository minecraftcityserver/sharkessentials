package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class PingCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				p.sendMessage("�6Server �8>> Dein Ping: �c"+((org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer)p).getHandle().ping);
			} else {
				if(p.hasPermission("sharkessentials.vip.ping")) {
					Player p2 = Bukkit.getPlayer(args[0]);
					
					if(p2 != null && p2.isOnline()) {
						
						p.sendMessage("�6Server �8>> Ping von �5"+p2.getName()+"�8: �c"+((org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer)p2).getHandle().ping);
						
					} else {
						p.sendMessage(SharkEssentials.op);
					}
					
				} else {
					p.sendMessage(SharkEssentials.se+"�4FEHLER�8: �cDu ben�tigst �6VIP �cRechte!");
				}
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}
	
	

}
