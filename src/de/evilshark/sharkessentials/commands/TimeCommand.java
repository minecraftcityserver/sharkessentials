package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class TimeCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				p.sendMessage(SharkEssentials.se+"Momentane Uhrzeit: �5"+p.getWorld().getTime());
			} else if(args.length == 1 ) {
				if(args[0].equalsIgnoreCase("day")) {
					p.sendMessage(SharkEssentials.se+"Es ist jetzt Tag!");
					p.getWorld().setTime(0);
				} else if(args[0].equalsIgnoreCase("night")) {
					p.sendMessage(SharkEssentials.se+"Es ist jetzt Nacht!");
					p.getWorld().setTime(18000);
				} else {
					int time = Integer.parseInt(args[0]);
					p.sendMessage(SharkEssentials.se+"Zeit wurde auf �5"+time+" �8gesetzt!");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "time set "+time);
				}
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}

		return true;
	}

}
