package de.evilshark.sharkessentials.commands;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class CommandSpyCommand implements CommandExecutor {

	public static ArrayList<String> cspy = new ArrayList<String>();	

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				
				if(cspy.contains(p.getName())) {
					cspy.remove(p.getName());
					p.sendMessage(SharkEssentials.se+"Commandspy wurde §cdeaktiviert§8!");
				} else {
					cspy.add(p.getName());
					p.sendMessage(SharkEssentials.se+"Commandspy wurde §2aktiviert§8!");
				}
				
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}

}
