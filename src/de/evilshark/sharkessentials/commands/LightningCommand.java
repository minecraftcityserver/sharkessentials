package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class LightningCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				p.getWorld().strikeLightning(p.getEyeLocation());
			} else if(args.length == 1) {
				Player p2 = Bukkit.getPlayer(args[0]);
				
				if(p2 !=null && p2.isOnline()) {
					p2.getWorld().strikeLightning(p2.getLocation());
					p.sendMessage(SharkEssentials.se+"Der Blitz ist eingeschlagen!");
				} else {
					p.sendMessage(SharkEssentials.op);
				}
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}
	
	

}
