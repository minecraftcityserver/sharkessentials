package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class XPCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				p.sendMessage(SharkEssentials.se+"XP: �5"+p.getExp());
			} else if(args.length == 1) {
				
				try {
					int xp = Integer.parseInt(args[0]);
					p.giveExpLevels(xp);
					p.sendMessage(SharkEssentials.se+"Deine XP wurde hinzugef�gt!");
				} catch(Exception exe) {
					p.sendMessage(SharkEssentials.se+"�4FEHLER�8: �cBitte verwende einen anderen Wert!");
				}
				
			} else if(args.length == 2) {
				Player p2 = Bukkit.getPlayer(args[0]);
			
				if(p2 != null && p2.isOnline()) {

					
					try {
						int xp = Integer.parseInt(args[1]);
						p2.giveExpLevels(xp);
						p.sendMessage(SharkEssentials.se+"Die XP wurde hinzugef�gt!");
					} catch(Exception exe) {
						p.sendMessage(SharkEssentials.se+"�4FEHLER�8: �cBitte verwende einen anderen Wert!");
					}
				} else {
					p.sendMessage(SharkEssentials.op);
				}
				
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}

}
