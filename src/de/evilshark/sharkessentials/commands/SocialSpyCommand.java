package de.evilshark.sharkessentials.commands;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class SocialSpyCommand implements CommandExecutor {

	public static ArrayList<String> msgspy = new ArrayList<String>();	
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			
			Player p = (Player) cs;
			
			if(args.length == 0) {
				
				if(msgspy.contains(p.getName())) {
					msgspy.remove(p.getName());
					p.sendMessage(SharkEssentials.se+"Du kannst nun nicht mehr private Nachrichten lesen!");
				} else {
					msgspy.add(p.getName());
					p.sendMessage(SharkEssentials.se+"Du kannst nun private Nachrichten lesen!");
				}
				
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
 			
		
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		
		return true;
	}
	
	

}
