package de.evilshark.sharkessentials.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class FreezeCommand implements CommandExecutor {

	public static ArrayList<String> freeze = new ArrayList<String>();
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			for(Player all:Bukkit.getOnlinePlayers()) {
			
			if(args.length == 0) {
					
				if(freeze.contains("freezed")) {
					freeze.remove("freezed");
					Bukkit.broadcastMessage(SharkEssentials.se+"Alle Spieler wurden entfreezt!");
				} else {
					freeze.add("freezed");
					freeze.remove("freezed");
					Bukkit.broadcastMessage(SharkEssentials.se+"Alle Spieler wurden gefreezt!");
					all.closeInventory();
				}
				
				
			} else if(args.length == 1) {
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}
	

}
