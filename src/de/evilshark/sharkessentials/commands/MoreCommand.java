package de.evilshark.sharkessentials.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.evilshark.sharkessentials.SharkEssentials;

public class MoreCommand implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 1) {
				
				ItemStack item = new ItemStack(p.getItemInHand().getType());
				
				try{
					int amount = Integer.parseInt(args[0]);
					if(amount <= 64) {
						
						if(p.getItemInHand() != null) {
							item.setAmount(amount);
							p.getInventory().addItem(item);
							p.sendMessage(SharkEssentials.se+"Die Bl�cke des Typs �5"+item.getType()+" �8wurden hinzugef�gt!");
						} else {
							p.sendMessage(SharkEssentials.se+"�4FEHLER�8: �cBitte verwende einen anderen Block!");
						}
						
					} else {
						p.sendMessage(SharkEssentials.se+"�4FEHLER�8: �cBitte verwende einen anderen Wert!");
					}
				} catch(Exception exe) {
					p.sendMessage(SharkEssentials.se+"�4FEHLER�8: �cBitte verwende einen anderen Wert!");
				}
				
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}
	
	

}
