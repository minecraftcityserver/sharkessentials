package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class ClearServerCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				
				Bukkit.broadcastMessage("�6Server �8>> Der Server wird in �510 �8Sekunden gecleart!");
				
				Bukkit.getScheduler().scheduleSyncDelayedTask(SharkEssentials.instance, new Runnable() {
					
					@Override
					public void run() {
						
						Bukkit.broadcastMessage("�6Server �8>> Der Server wurde gecleart!");
						Bukkit.broadcastMessage("�6Server �8>> Alle Items wurden gel�scht!");
						Bukkit.broadcastMessage("�6Server �8>> Alle XP-Orbs wurden gel�scht!");
						
						for(Entity ent : p.getWorld().getEntities()) {
							
							if(ent instanceof Item || ent instanceof ExperienceOrb) {
								
								ent.remove();
							}
							
						}
						
					}
					
				}, 200);
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		
		return true;
	}

}
