package de.evilshark.sharkessentials.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import de.evilshark.sharkessentials.SharkEssentials;

public class TrollCommand implements CommandExecutor {

	public static ArrayList<String> troll = new ArrayList<String>();
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				
				if(troll.contains(p.getName())) {
					troll.remove(p.getName());
					p.sendMessage(SharkEssentials.se+"Du bist nun nicht mehr im �5TROLL �8Modus!");
					p.removePotionEffect(PotionEffectType.INVISIBILITY);
					p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
					p.removePotionEffect(PotionEffectType.FAST_DIGGING);
					p.playSound(p.getLocation(), Sound.ENTITY_IRONGOLEM_HURT, 1, 1);
					p.setGameMode(GameMode.SURVIVAL);
				} else {
					troll.add(p.getName());
					p.sendMessage(SharkEssentials.se+"Du bist nun im �5TROLL �8Modus!");
					p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 1000000, 100000));
					p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 1000000, 100000));
					p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 1000000, 100000));
					p.playSound(p.getLocation(), Sound.ENTITY_IRONGOLEM_HURT, 1, 1);
					p.setGameMode(GameMode.CREATIVE);
				}
				
				
				
			} else if(args.length == 1) {
				Player p2 = Bukkit.getPlayer(args[0]);
				if(p2 != null && p2.isOnline()) {
					
					if(troll.contains(p2.getName())) {
						troll.remove(p2.getName());
						p2.sendMessage(SharkEssentials.se+"Du bist nun nicht mehr im �5TROLL �8Modus!");
						p.sendMessage(SharkEssentials.se+"Der Spieler �6"+p2.getName()+" �8ist nun nicht mehr im �5TROLL �8Modus!");
						p2.removePotionEffect(PotionEffectType.INVISIBILITY);
						p2.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
						p.removePotionEffect(PotionEffectType.FAST_DIGGING);
						p2.playSound(p.getLocation(), Sound.ENTITY_IRONGOLEM_HURT, 1, 1);
						p.playSound(p.getLocation(), Sound.ENTITY_IRONGOLEM_HURT, 1, 1);
						p2.setGameMode(GameMode.SURVIVAL);
					} else {
						troll.add(p2.getName());
						p2.sendMessage(SharkEssentials.se+"Du bist nun im �5TROLL �8Modus!");
						p.sendMessage(SharkEssentials.se+"Der Spieler �6"+p2.getName()+" �8ist nun im �5TROLL �8Modus!");
						p2.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 1000000, 100000));
						p2.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 1000000, 100000));
						p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 1000000, 100000));
						p2.playSound(p.getLocation(), Sound.ENTITY_IRONGOLEM_HURT, 1, 1);
						p.playSound(p.getLocation(), Sound.ENTITY_IRONGOLEM_HURT, 1, 1);
						p2.setGameMode(GameMode.CREATIVE);
					}
				}
				
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}

}
