package de.evilshark.sharkessentials.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class SpeedCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 1) {
				if(p.isFlying()) {
					
					try{
						float speed = Float.parseFloat(args[0]);
						p.setFlySpeed(speed);
						p.sendMessage(SharkEssentials.se+"Dein Fly Speed ist nun �5"+speed);
					} catch(Exception exe) {
						p.sendMessage(SharkEssentials.se+"�4FEHLER�8: �cBitte verwende einen anderen Wert!");
					}
					
				} else {
					try{
						float speed = Float.parseFloat(args[0]);
						p.setWalkSpeed(speed);
						p.sendMessage(SharkEssentials.se+"Dein Walk Speed ist nun �5"+speed);
					} catch(Exception exe) {
						p.sendMessage(SharkEssentials.se+"�4FEHLER�8: �cBitte verwende einen anderen Wert!");
					}
				}
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}

}
