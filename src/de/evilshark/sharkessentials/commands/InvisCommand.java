package de.evilshark.sharkessentials.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import de.evilshark.sharkessentials.SharkEssentials;

public class InvisCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 1) {
				if(args[0].equalsIgnoreCase("true")) {
					p.sendMessage(SharkEssentials.se+"Du bist nun unsichtbar!");
					p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 100000, 10000000));
										
				} else if(args[0].equalsIgnoreCase("false")) {
					p.sendMessage(SharkEssentials.se+"Du bist nun sichtbar!");
					p.removePotionEffect(PotionEffectType.INVISIBILITY);					
					
				} else {
					p.sendMessage(SharkEssentials.af);
				}
				
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}
	
	

}
