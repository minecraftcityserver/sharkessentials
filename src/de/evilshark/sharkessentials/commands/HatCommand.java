package de.evilshark.sharkessentials.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.evilshark.sharkessentials.SharkEssentials;

public class HatCommand implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				
				if(p.getItemInHand() != null && p.getItemInHand().getType().isBlock()) {
					ItemStack item = new ItemStack (p.getItemInHand().getType());
					p.getInventory().setHelmet(item);
					p.sendMessage(SharkEssentials.se+"Block angezogen!");
				} else {
					p.sendMessage(SharkEssentials.se+"�4FEHLER�8: �cBitte w�hle einen Block aus!");
				}
								
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}

}
