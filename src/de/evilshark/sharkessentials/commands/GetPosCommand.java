package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class GetPosCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				p.sendMessage(SharkEssentials.se+"Du befindest dich bei: �5X: "+p.getLocation().getBlockX()+" �5Y: "+p.getLocation().getBlockY()+" �5Z: "+p.getLocation().getBlockZ());
			} else if(args.length == 1) {
				
				Player p2 = Bukkit.getPlayer(args[0]);
				
				if(p2 != null && p2.isOnline()) {
					
					if(p.hasPermission("sharkessentials.commands.getpos.other")) {
					
					p.sendMessage(SharkEssentials.se+"Der Spieler �5"+p2.getName()+" �8befindet sich bei: �5X: "+p2.getLocation().getBlockX()+" �5Y: "+p2.getLocation().getBlockY()+" �5Z: "+p2.getLocation().getBlockZ());
					
					} else {
						p.sendMessage(SharkEssentials.se+"�4FEHLER�8: �cDu hast nicht gen�gend Rechte!");
					}
				} else {
					p.sendMessage(SharkEssentials.op);
				}
				
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}

}
