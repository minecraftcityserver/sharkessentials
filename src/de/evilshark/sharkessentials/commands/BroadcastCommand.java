package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class BroadcastCommand implements CommandExecutor {
	String broadcast = " ";
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length >= 1) {
				
				for(int i = 0; i < args.length; i++) {
					broadcast = broadcast + args[i] + " ";
				}
				Bukkit.broadcastMessage("�6Server �8>> �4"+broadcast);
				broadcast = "";
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		 
		return true;
	}

}
