package de.evilshark.sharkessentials.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;



public class AfkCommand implements CommandExecutor {

	public static ArrayList<String> afk = new ArrayList<String>();	

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				if(afk.contains(p.getName())) {
					afk.remove(p.getName());
					Bukkit.broadcastMessage(SharkEssentials.se+"Der Spieler �6"+p.getName()+" �8ist nun nicht mehr AFK!");
					p.sendMessage(SharkEssentials.se+"Du bist nun nicht mehr AFK!");
					p.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT, 1, 1);
				} else {
					afk.add(p.getName());
					Bukkit.broadcastMessage(SharkEssentials.se+"Der Spieler �6"+p.getName()+" �8ist nun AFK!");
					p.sendMessage(SharkEssentials.se+"Du bist nun AFK!");
					p.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT, 1, 1);
				}
				
			} else if(args.length == 1) {
				Player p2 = Bukkit.getPlayer(args[0]);
				if(p2 != null && p2.isOnline()) {
					if(afk.contains(p2.getName())) {
						afk.remove(p2.getName());
						Bukkit.broadcastMessage(SharkEssentials.se+"Der Spieler �6"+p2.getName()+" �8ist nun nicht mehr AFK!");
						p2.sendMessage(SharkEssentials.se+"Du bist nun nicht mehr AFK!");
						p.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT, 1, 1);
						p2.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT, 1, 1);
					} else {
						afk.add(p2.getName());
						Bukkit.broadcastMessage(SharkEssentials.se+"Der Spieler �6"+p2.getName()+" �8ist nun AFK!");
						p2.sendMessage(SharkEssentials.se+"Du bist nun AFK!");
						p.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT, 1, 1);
						p2.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT, 1, 1);
					}
					
					
				} else {
					p.sendMessage(SharkEssentials.op);
				}
				
			} else {
				p.sendMessage(SharkEssentials.af);;
			}
			
			
		} else {
			cs.sendMessage(SharkEssentials.se);
		}
		
		
		return true;
	}

}
