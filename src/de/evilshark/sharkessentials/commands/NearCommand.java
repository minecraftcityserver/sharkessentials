package de.evilshark.sharkessentials.commands;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class NearCommand implements CommandExecutor {
	
	public static ArrayList<String> found = new ArrayList<String>();
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				
				for(Entity nearEnt : p.getNearbyEntities(1000D, 1000D, 1000D)) {
					
					if(nearEnt instanceof Player) {
						Player detected = (Player) nearEnt;
						double dist = p.getLocation().distance(detected.getLocation());
						
						if(dist <= 150) {
							
							found.add("found");
							int loc = (int) p.getLocation().distance(detected.getLocation());
							
							
							
							p.sendMessage(SharkEssentials.se+"Der/Die Spieler �5"+detected.getName()+" �8ist/sind in der N�he! �cEntfernung: "+loc);
						} else {
							if(found.contains("found")) {
								p.sendMessage(SharkEssentials.se+"�cKeine weiteren Spieler in der N�he!");	
								found.remove("found");
							}
							
						}
						
				}
					
					
				} 
				

					
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}

}
