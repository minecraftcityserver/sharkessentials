package de.evilshark.sharkessentials.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class PVPCommand implements CommandExecutor {

	public static ArrayList<String> pvp = new ArrayList<String>();	
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				
				if(pvp.contains("pvp")) {
					pvp.remove("pvp");
					Bukkit.broadcastMessage("�6Server �8>> PVP ist nun wieder �2AKTIV �8!");
					p.getWorld().setPVP(true);
				} else {
					pvp.add("pvp");
					Bukkit.broadcastMessage("�6Server �8>> PVP ist nun �4DEAKTIVIERT �8!");
					p.getWorld().setPVP(false);
				}
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}

}
