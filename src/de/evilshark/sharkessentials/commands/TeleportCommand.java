package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class TeleportCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 1) {
				
				Player p2 = Bukkit.getPlayer(args[0]);
				
				if(p2 != null && p2.isOnline()) {
					
					p.teleport(p2.getLocation());
					p.sendMessage(SharkEssentials.se+"Du wurdest zu �5"+p2.getName()+" �8teleportiert!");
					
				} else {
					p.sendMessage(SharkEssentials.op);
				}
				
			} else if(args.length == 2) {
				
				Player p2 = Bukkit.getPlayer(args[0]);
				Player p3 = Bukkit.getPlayer(args[1]);
				
				if(p2 != null && p2.isOnline()) {
					
					if(p3 != null && p3.isOnline()) {
						
						p2.teleport(p3.getLocation());
						p.sendMessage(SharkEssentials.se+"Der Spieler �5"+p2.getName()+" �8wurde zu �5"+p3.getName()+" �8teleportiert!");
						p2.sendMessage(SharkEssentials.se+"Du wurdest zu �5"+p3.getName()+" �8teleportiert!");
					} else {
						p.sendMessage(SharkEssentials.op);
					}
					
				} else {
					p.sendMessage(SharkEssentials.op);
				}
				
			} else if(args.length == 0) {
				p.sendMessage(SharkEssentials.af);
			} else {
				Bukkit.dispatchCommand(p, "tp "+p.getName()+" "+args[0]+" "+args[1]+" "+args[2]);
			}
			
			
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		
		return true;
	}

}
