package de.evilshark.sharkessentials.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class FlyCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 0) {
				if(p.getAllowFlight() == true) {
					p.setFlying(false);
					p.setAllowFlight(false);
					p.sendMessage(SharkEssentials.se+"Du kannst nun nicht mehr fliegen!");
				} else {
					p.setAllowFlight(true);
					p.sendMessage(SharkEssentials.se+"Du kannst nun fliegen!");
				}
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
	
		return true;
	}

}
