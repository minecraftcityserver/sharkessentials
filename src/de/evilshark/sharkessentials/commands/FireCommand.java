package de.evilshark.sharkessentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkessentials.SharkEssentials;

public class FireCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs instanceof Player) {
			Player p = (Player) cs;
			
			if(args.length == 2) {
				Player p2 = Bukkit.getPlayer(args[0]);
				
				if(p2 !=null && p2.isOnline()) {
					int fire = Integer.parseInt(args[1]);
					
					p.sendMessage(SharkEssentials.se+"Der Spieler �5"+p2.getName()+" �8brennt nun!");
					p2.setFireTicks(fire * 20);
					
				} else {
					p.sendMessage(SharkEssentials.op);
				}
				
			} else {
				p.sendMessage(SharkEssentials.af);
			}
			
		} else {
			cs.sendMessage(SharkEssentials.cc);
		}
		
		return true;
	}

}
