package de.evilshark.sharkessentials.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import de.evilshark.sharkessentials.SharkEssentials;
import de.evilshark.sharkessentials.commands.CommandSpyCommand;

public class CommandSpyListener implements Listener {

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		
		Player p = (Player) e.getPlayer();
		
		if(!(CommandSpyCommand.cspy.contains(p.getName()))) {
			
			for(int i = 0; i <  CommandSpyCommand.cspy.size(); i++) {
				
				Player admin = e.getPlayer().getServer().getPlayerExact(CommandSpyCommand.cspy.get(i));
				
				admin.sendMessage(SharkEssentials.se+"Der Spieler §5"+p.getName()+" §8hat folgeneden Command ausgeführt: §5"+e.getMessage());
				
			}
			
		}
		
	}

}
