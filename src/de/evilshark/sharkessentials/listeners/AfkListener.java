package de.evilshark.sharkessentials.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import de.evilshark.sharkessentials.SharkEssentials;
import de.evilshark.sharkessentials.commands.AfkCommand;

public class AfkListener implements Listener {
	
	@EventHandler
	public void onWalk(PlayerMoveEvent e) {
		
		Player p = (Player) e.getPlayer();
		
		if(AfkCommand.afk.contains(p.getName())) {
			AfkCommand.afk.remove(p.getName());
			Bukkit.broadcastMessage(SharkEssentials.se+"Der Spieler �6"+p.getName()+" �8ist nun nicht mehr AFK!");
			p.sendMessage(SharkEssentials.se+"Du bist nun nicht mehr AFK!");
			p.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT, 1, 1);
			
		}
		
	}


}
