package de.evilshark.sharkessentials.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import de.evilshark.sharkessentials.commands.GodCommand;

public class GodListener implements Listener {
	
	@EventHandler
	public void onPlayerDamage(EntityDamageEvent e) {
		
		if(e.getEntity() instanceof Player) {
			Player p = (Player)e.getEntity();
			
			if(GodCommand.god.contains(p.getName())) {
				e.setCancelled(true);
			}
		}
		
	}	

}
