package de.evilshark.sharkessentials.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.help.HelpTopic;

public class UnknownCommandListener implements Listener {
	
	@EventHandler (priority = EventPriority.NORMAL)
	public void onUnknown(PlayerCommandPreprocessEvent e) {
		Player p = (Player) e.getPlayer();
		
		
		if(!(e.isCancelled())) {
			
			
			String msg = e.getMessage().split(" ")[0];
			HelpTopic topic = Bukkit.getServer().getHelpMap().getHelpTopic(msg);
			
				if(topic == null) {
					p.sendMessage("�6Server �8>> �cDieser Command wurde nicht gefunden!");
					e.setCancelled(true);
				}
			
			
		}
		
	}

}
