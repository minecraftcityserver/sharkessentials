package de.evilshark.sharkessentials.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class LeaveListener implements Listener {
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		
		e.setQuitMessage("§6Server §8>> Der Spieler §5"+e.getPlayer().getName()+" §8hat den Server §4verlassen§8!");
		
	}

}
