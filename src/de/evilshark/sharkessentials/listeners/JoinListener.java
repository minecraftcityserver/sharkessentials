package de.evilshark.sharkessentials.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		
		e.setJoinMessage("§6Server §8>> Der Spieler §5"+e.getPlayer().getName()+" §8ist dem Server §2beigetreten§8!");
		
	}

}
