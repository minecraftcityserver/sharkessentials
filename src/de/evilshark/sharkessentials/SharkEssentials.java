package de.evilshark.sharkessentials;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import de.evilshark.sharkessentials.commands.AfkCommand;
import de.evilshark.sharkessentials.commands.BackPackCommand;
import de.evilshark.sharkessentials.commands.BroadcastCommand;
import de.evilshark.sharkessentials.commands.ClearServerCommand;
import de.evilshark.sharkessentials.commands.CommandSpyCommand;
import de.evilshark.sharkessentials.commands.FeedCommand;
import de.evilshark.sharkessentials.commands.FireCommand;
import de.evilshark.sharkessentials.commands.FlyCommand;
import de.evilshark.sharkessentials.commands.FreezeCommand;
import de.evilshark.sharkessentials.commands.GamemodeCommand;
import de.evilshark.sharkessentials.commands.GetPosCommand;
import de.evilshark.sharkessentials.commands.GodCommand;
import de.evilshark.sharkessentials.commands.HatCommand;
import de.evilshark.sharkessentials.commands.HealCommand;
import de.evilshark.sharkessentials.commands.HungerCommand;
import de.evilshark.sharkessentials.commands.InvisCommand;
import de.evilshark.sharkessentials.commands.InvseeCommand;
import de.evilshark.sharkessentials.commands.KillCommand;
import de.evilshark.sharkessentials.commands.MSGCommand;
import de.evilshark.sharkessentials.commands.MoreCommand;
import de.evilshark.sharkessentials.commands.MotdCommand;
import de.evilshark.sharkessentials.commands.PingCommand;
import de.evilshark.sharkessentials.commands.TrollCommand;
import de.evilshark.sharkessentials.commands.XPCommand;
import de.evilshark.sharkessentials.freeze.Freeze2Listener;
import de.evilshark.sharkessentials.freeze.Freeze3Listener;
import de.evilshark.sharkessentials.freeze.FreezeListener;
import de.evilshark.sharkessentials.listeners.Afk2Listener;
import de.evilshark.sharkessentials.listeners.AfkListener;
import de.evilshark.sharkessentials.listeners.CommandSpyListener;
import de.evilshark.sharkessentials.listeners.GodListener;
import de.evilshark.sharkessentials.listeners.JoinListener;
import de.evilshark.sharkessentials.listeners.LeaveListener;
import de.evilshark.sharkessentials.listeners.MsgSpyListener;
import de.evilshark.sharkessentials.listeners.UnknownCommandListener;

public class SharkEssentials extends JavaPlugin {
	public static SharkEssentials instance;

	public static String se = "�1SharkEssentials �8>> ";
	public static String cc = "�1SharkEssentials �8>> �4FEHLER: �cDu darfst diesen Comamnd nur als Spieler ausf�hren!";
	public static String af = "�1SharkEssentials �8>> �4FEHLER: �cBitte �berpr�fe die Argrumente!";
	public static String op = "�1SharkEssentials �8>> �4FEHLER: �cDer Spieler wurde nicht gefunden!";
	public static String np = "�6Server �8>> �cDu hast nicht gen�gend Rechte!";
	
	
	
	
	@Override
	public void onEnable() {
		System.out.println(se+"�5SharkEssentials �8wurde �2gestartet�8!");
		instance = this;
		for(Player op:Bukkit.getOnlinePlayers()) {
			
			if(op.hasPermission("sharkessentials.status.show")) {
				op.sendMessage(se+"�5SharkEssentials �8wurde �2gestartet�8!");
			}
			
		}
		
		this.getCommand("kill").setExecutor(new KillCommand());
		this.getCommand("gamemode").setExecutor(new GamemodeCommand());
		this.getCommand("heal").setExecutor(new HealCommand());
		this.getCommand("god").setExecutor(new GodCommand());
		this.getCommand("hunger").setExecutor(new HungerCommand());
		this.getCommand("invisible").setExecutor(new InvisCommand());
		this.getCommand("afk").setExecutor(new AfkCommand());
		this.getCommand("troll").setExecutor(new TrollCommand());
		//this.getCommand("time").setExecutor(new TimeCommand());
		this.getCommand("freeze").setExecutor(new FreezeCommand());
		this.getCommand("message").setExecutor(new MSGCommand());
		this.getCommand("motd").setExecutor(new MotdCommand());
		this.getCommand("broadcast").setExecutor(new BroadcastCommand());
		this.getCommand("fire").setExecutor(new FireCommand());
		this.getCommand("fly").setExecutor(new FlyCommand());
		//this.getCommand("lightning").setExecutor(new LightningCommand());
		//this.getCommand("pvp").setExecutor(new PVPCommand());
		this.getCommand("xp").setExecutor(new XPCommand());
		this.getCommand("feed").setExecutor(new FeedCommand());
		this.getCommand("hat").setExecutor(new HatCommand());
		this.getCommand("ping").setExecutor(new PingCommand());
		this.getCommand("more").setExecutor(new MoreCommand());
		//this.getCommand("repair").setExecutor(new RepairCommand());
		this.getCommand("getpos").setExecutor(new GetPosCommand());
		//this.getCommand("speed").setExecutor(new SpeedCommand());
		//this.getCommand("near").setExecutor(new NearCommand());
		this.getCommand("invsee").setExecutor(new InvseeCommand());
		this.getCommand("backpack").setExecutor(new BackPackCommand());
		this.getCommand("commandspy").setExecutor(new CommandSpyCommand());
		//this.getCommand("socialspy").setExecutor(new SocialSpyCommand());
		this.getCommand("clearserver").setExecutor(new ClearServerCommand());
		//this.getCommand("teleport").setExecutor(new TeleportCommand());
		
		PluginManager pm = this.getServer().getPluginManager();
		
		pm.registerEvents(new GodListener(), this);
		pm.registerEvents(new AfkListener(), this);
		pm.registerEvents(new Afk2Listener(), this);
		pm.registerEvents(new FreezeListener(), this);
		pm.registerEvents(new Freeze2Listener(), this);
		pm.registerEvents(new Freeze3Listener(), this);
		pm.registerEvents(new CommandSpyListener(), this);
		pm.registerEvents(new MsgSpyListener(), this);
		pm.registerEvents(new JoinListener(), this);
		pm.registerEvents(new LeaveListener(), this);
		pm.registerEvents(new UnknownCommandListener(), this);
		
	}
	
	
	

}
